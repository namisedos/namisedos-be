using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Namisedos_BE.Controllers;
using Namisedos_BE.Entities;
using Namisedos_BE.Models;
using Namisedos_BE.Repositories;
using Namisedos_BE.Services;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Namisedos_BE.Helpers;
using Namisedos_BE.Mappers;
using Xunit;

namespace UnitTests
{
    public class DataContextTests
    {

        [Fact]
        public void DataContext_Constructor_Return_Not_Null()
        {
            var dbContext = new DataContext("test");
            dbContext.Should().NotBeNull();
        }
        [Fact]
        public void DataContext_ConnectionString_Initialized()
        {
            var dbContext = new DataContext("test");
            var dbContextOptionsBuilder = new DbContextOptionsBuilder();
            dbContext.UseSqlServerConnectionString(dbContextOptionsBuilder);
            dbContextOptionsBuilder.IsConfigured.Should().BeTrue();

        }
        [Fact]
        public void DataContext_ConnectionString_Not_Initialized()
        {
            var dbContext = new DataContext();
            var dbContextOptionsBuilder = new DbContextOptionsBuilder();
            dbContext.UseSqlServerConnectionString(dbContextOptionsBuilder);
            dbContextOptionsBuilder.IsConfigured.Should().BeFalse();

        }


    }
}