using System;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Namisedos_BE.Controllers;
using Namisedos_BE.Entities;
using Namisedos_BE.Models;
using Namisedos_BE.Services;
using System.Collections.Generic;
using Xunit;

namespace UnitTests
{
    public class UserControllerTests
    {
        [Fact]
        public void UserController_CreateUser_Success200()
        {
            //Arrange

            var userController = new UserController(UserServiceTests.GetServiceMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var registerRequest = new RegisterRequestModel
            {
                Username = "NotFound"
            };

            //Act
            var result = userController.Register(registerRequest) as OkObjectResult;

            //Assert
            result.Should().NotBeNull();
            result?.Value.Should().NotBeNull();
            result?.StatusCode.Should().Be(200);
            var resultUser = (User)result?.Value!;
            resultUser.Username.Should().Be("Created");
        }
        [Fact]
        public void UserController_CreateUser_Username_Exists_Fail400()
        {
            //Arrange
            var userController = new UserController(UserServiceTests.GetServiceMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var registerRequest = new RegisterRequestModel("fail400@", "Found", "password");

            //Act
            var result = userController.Register(registerRequest) as BadRequestObjectResult;
            var expected = new BadRequestObjectResult(new
            { field = "username", status = "400", message = "User with this username already exists" });

            //Assert
            result.Should().NotBeNull();
            result?.StatusCode.Should().Be(expected.StatusCode);
        }
        [Fact]
        public void UserController_Test()
        {
            //Arrange

            var userController = new UserController(UserServiceTests.GetServiceMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);

            //Act
            var result = userController.Test();

            //Assert
            result.Should().NotBeNull();
            result.Should().Be("Veikia");
        }
        [Fact]
        public void UserController_TestSecret()
        {
            //Arrange

            var userController = new UserController(UserServiceTests.GetServiceMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);

            //Act
            var result = userController.TestSecret();

            //Assert
            result.Should().NotBeNull();
            result.Should().Be("Veikia Secret");
        }

        [Fact]
        public void UserController_LoginUser_Success200()
        {
            //Arrange

            var userController = new UserController(UserServiceTests.GetServiceMock().Object, new Mock<IMapper>().Object);
            var loginRequest = new LoginRequestModel
            (
               "Found",
                "test"
            );

            //Act
            var result = userController.Login(loginRequest) as OkObjectResult;

            //Assert
            result.Should().NotBeNull();
            result?.Value.Should().NotBeNull();
            result?.StatusCode.Should().Be(200);
            var resultUser = (User)result?.Value!;
            resultUser.Password.Should().Be(loginRequest.Password);
        }
        [Fact]
        public void UserController_LoginUser_Username_Not_Exists_Fail400()
        {
            //Arrange
            var userController = new UserController(UserServiceTests.GetServiceMock().Object, new Mock<IMapper>().Object);
            var loginRequest = new LoginRequestModel("NotFound", "test");

            //Act
            var result = userController.Login(loginRequest) as BadRequestObjectResult;
            var expected = new BadRequestObjectResult(new
            { field = "username", status = "400", message = "User with this username does not exists" });

            //Assert
            result.Should().NotBeNull();
            result?.StatusCode.Should().Be(expected.StatusCode);
        }
        [Fact]
        public void UserController_LoginUser_Password_Wrong_Password400()
        {
            //Arrange
            var userController = new UserController(UserServiceTests.GetServiceMock().Object, new Mock<IMapper>().Object);
            var loginRequest = new LoginRequestModel("Found", "password");

            //Act
            var result = userController.Login(loginRequest) as BadRequestObjectResult;
            var expected = new BadRequestObjectResult(new
            { field = "password", status = "400", message = "Wrong password" });

            //Assert
            result.Should().NotBeNull();
            result?.StatusCode.Should().Be(expected.StatusCode);
        }


        [Fact]
        public void UserController_GetAll_Found()
        {
            //Arrange
            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetAll()).Returns(new List<User>() { new() { Username = "Test" }, new() { Username = "Test2" } });
            var userController = new UserController(userServiceMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            //Act
            var result = userController.GetAll() as OkObjectResult;
            //Assert
            result.Should().NotBeNull();
            result?.Value.Should().NotBeNull();
            result?.StatusCode.Should().Be(200);
            var resultUsers = result?.Value as List<User>;
            resultUsers?.Count.Should().Be(2);

        }

        [Fact]
        public void UserController_Update_UserNotFound()
        {
            //Arrange
            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.FindUserByUsername(It.IsAny<string>())).Returns((User?)null);
            var userController = new UserController(userServiceMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var updateRequest = new UpdateRequestModel
            {
                Username = "NotFoundUpdate"
            };
            //Act
            var result = userController.Update(updateRequest) as BadRequestObjectResult;
            result.Should().NotBeNull();
            result?.StatusCode.Should().Be(400);
            //Assert


        }

        [Fact]
        public void UserController_Update_Success()
        {
            //Arrange
            var userServiceMock = new Mock<IUserService>();
            userServiceMock.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(new User() { Username = "Test" });
            userServiceMock.Setup(x => x.UpdateUser(It.IsAny<User>())).Returns(new User() { Username = "Updated" });
            var userController = new UserController(userServiceMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var updateRequest = new UpdateRequestModel
            {
                Username = "NotFoundUpdate"
            };
            //Act
            var result = userController.Update(updateRequest);
            var resultObjectResult = result as OkObjectResult;
            //Assert
            result.Should().NotBeNull();
            resultObjectResult.Should().NotBeNull();
            resultObjectResult?.StatusCode.Should().Be(200);
            var resultUser = resultObjectResult?.Value as User;
            resultUser?.Username.Should().Be("Updated");
        }

    }
}