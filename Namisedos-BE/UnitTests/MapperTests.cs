using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Namisedos_BE.Controllers;
using Namisedos_BE.Entities;
using Namisedos_BE.Models;
using Namisedos_BE.Repositories;
using Namisedos_BE.Services;
using System.Collections.Generic;
using Namisedos_BE.Mappers;
using Xunit;

namespace UnitTests
{
    public class MapperTests
    {

        [Fact]
        public void RegisterRequestModel_To_User_Mapper()
        {
            var mapperMock = RegisterRequestModelToUserMapperMock();
            var registerRequest = new RegisterRequestModel("success200@", "success200", "password");
            var user = mapperMock.Object.Map<User>(registerRequest);
            user.Username.Should().Be(registerRequest.Username);
            user.Email.Should().Be(registerRequest.Email);
            user.Password.Should().Be(registerRequest.Password);
        }
        public static Mock<IMapper> RegisterRequestModelToUserMapperMock(Mock<IMapper>? mapperMock = null)
        {
            mapperMock ??= new Mock<IMapper>();
            mapperMock
            .Setup(x => x.Map<User>(It.IsAny<RegisterRequestModel>()))
            .Returns((RegisterRequestModel source) =>
                new User()
                {
                    Email = source.Email,
                    Username = source.Username,
                    Password = source.Password
                }
            );
            return mapperMock;
        }
        
    }
}