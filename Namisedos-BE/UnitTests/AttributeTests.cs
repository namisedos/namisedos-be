﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Namisedos_BE.Attributes;
using Namisedos_BE.Models;
using Xunit;

namespace UnitTests
{
    public class AttributeTests
    {

        [Fact]
        public void AuthrorizeAttribute_NoTokenFound()
        {
            var actionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext(),
                RouteData = new RouteData(),
                ActionDescriptor = new ActionDescriptor()
            };
            var authorizationFilterContext =
                new AuthorizationFilterContext(actionContext, new List<IFilterMetadata>());
            var attribute = new Authorize();

            // When
            attribute.OnAuthorization(authorizationFilterContext);
            var result = authorizationFilterContext.HttpContext.Response;
            // Then
            result.StatusCode.Should().Be((int)HttpStatusCode.ExpectationFailed);

        }
        [Fact]
        public void AuthrorizeAttribute_BadToken()
        {
            var actionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext(),
                RouteData = new RouteData(),
                ActionDescriptor = new ActionDescriptor()
            };
            actionContext.HttpContext.Request.Headers.Add("x-api-key", "123456");
            var authorizationFilterContext =
                new AuthorizationFilterContext(actionContext, new List<IFilterMetadata>());
            var attribute = new Authorize();

            // When
            attribute.OnAuthorization(authorizationFilterContext);
            var result = authorizationFilterContext.HttpContext.Response;
            // Then
            result.StatusCode.Should().Be((int)HttpStatusCode.Forbidden);

        }
        [Fact]
        public void AuthrorizeAttribute_Success()
        {
            var actionContext = new ActionContext()
            {
                HttpContext = new DefaultHttpContext(),
                RouteData = new RouteData(),
                ActionDescriptor = new ActionDescriptor()
            };
            actionContext.HttpContext.Request.Headers.Add("x-api-key", ApiKey.Key);
            var authorizationFilterContext =
                new AuthorizationFilterContext(actionContext, new List<IFilterMetadata>());
            var attribute = new Authorize();

            // When
            attribute.OnAuthorization(authorizationFilterContext);
            var headers = authorizationFilterContext.HttpContext.Response.Headers;
            // Then
            var result = headers.FirstOrDefault(s => s.Key == "authToken");
            result.Key.Should().Be("authToken");
            result.Value.ToString().Should().Be(ApiKey.Key);

        }
    }
}
