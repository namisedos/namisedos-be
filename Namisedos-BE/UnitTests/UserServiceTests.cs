using System;
using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Namisedos_BE.Controllers;
using Namisedos_BE.Entities;
using Namisedos_BE.Models;
using Namisedos_BE.Repositories;
using Namisedos_BE.Services;
using System.Collections.Generic;
using Microsoft.OpenApi.Any;
using Xunit;

namespace UnitTests
{
    public class UserServiceTests
    {
        [Fact]
        public void FindUsernameServiceTest_Found()
        {
            //Arrange
            var userService = new UserService(UserRepositoryTests.GetRepositoryMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var username = "Found";

            //Act
            var result = userService.FindUserByUsername(username);

            //Assert
            result.Should().NotBeNull();
        }

        [Fact]
        public void GetAllUsers_NotEmpty()
        {
            var repositoryMock = new Mock<IUserRepository>();
            repositoryMock.Setup(x => x.GetAll()).Returns(new List<User>() { new User() { Username = "NotEmpty" } });
            var userService = new UserService(repositoryMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object); var result = userService.GetAll();
            result.Should().NotBeNull();
            result.Should().BeOfType<List<User>>();
            result.Should().NotBeEmpty();
            result.Count.Should().Be(1);
        }
        [Fact]
        public void GetAllUsers_Empty()
        {
            var repositoryMock = new Mock<IUserRepository>();
            repositoryMock.Setup(x => x.GetAll()).Returns(new List<User>());
            var userService = new UserService(repositoryMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var result = userService.GetAll();
            result.Should().NotBeNull();
            result.Should().BeOfType<List<User>>();
            result.Should().BeEmpty();
            result.Count.Should().Be(0);
        }

        [Fact]
        public void UpdateUser_Success()
        {
            var repositoryMock = new Mock<IUserRepository>();
            var userInDb = new User()
            {
                Username = "Found2",
                Email = "user1@user2.net",
                Password = "123456"

            };

            var userToUpdate = new User()
            {
                Username = "Found2",
                Email = "Update@Update",
                Password = "123456",
                HomeAddress = new Coordinates()
                {
                    Latitude = 10.0,
                    Longitude = 11.0
                }
            };
            repositoryMock.Setup(x => x.GetById(It.IsAny<Guid>())).Returns(userInDb);
            repositoryMock.Setup(x => x.Update(It.IsAny<User>())).Returns(userInDb);
            var userService = new UserService(repositoryMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var result = userService.UpdateUser(userToUpdate);
            result.Should().NotBeNull();
            result.Should().BeOfType<User>();
            result?.Username.Should().Be(userToUpdate.Username);
            result?.Email.Should().Be(userToUpdate.Email);
            result?.Password.Should().Be(userToUpdate.Password);
            result?.HomeAddress.Should().NotBeNull();
            result?.HomeAddress?.Latitude.Should().Be(userToUpdate.HomeAddress.Latitude);
            result?.HomeAddress?.Longitude.Should().Be(userToUpdate.HomeAddress.Longitude);
        }

        [Fact]
        public void UpdateUser_UserNotFound()
        {
            var repositoryMock = new Mock<IUserRepository>();
            var userToUpdate = new User()
            {
                Username = "Found2",
                Email = "Update@Update",
                Password = "123456",
                HomeAddress = new Coordinates()
                {
                    Latitude = 10.0,
                    Longitude = 11.0
                }
            };
            repositoryMock.Setup(x => x.GetById(It.IsAny<Guid>())).Returns((User?) null);
            var userService = new UserService(repositoryMock.Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var result = userService.UpdateUser(userToUpdate);
            result.Should().BeNull();
            result?.Username.Should().BeNull();
            result?.Email.Should().BeNull();
            result?.Password.Should().BeNull();
            result?.HomeAddress.Should().BeNull();
            
        }

        [Fact]
        public void FindUsernameServiceTest_NotFound()
        {
            //Arrange
            var userService = new UserService(UserRepositoryTests.GetRepositoryMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var username = "NotFound";

            //Act
            var result = userService.FindUserByUsername(username);

            //Assert
            result.Should().Be(null);
        }

        [Fact]
        public void CreateUser()
        {
            var userService = new UserService(UserRepositoryTests.GetRepositoryMock().Object, MapperTests.RegisterRequestModelToUserMapperMock().Object);
            var createdUser = userService.CreateUser(new User());
            createdUser.Should().NotBeNull();
        }
        public static Mock<IUserService> GetServiceMock(Mock<IUserService>? userServiceMock = null)
        {
            userServiceMock ??= new Mock<IUserService>();
            userServiceMock
                .Setup(service => service
                    .FindUserByUsername(It.Is<string>(s => s.Contains("Found"))))
                .Returns(new User(){Password = "test"});
            userServiceMock
                .Setup(service => service
                    .FindUserByUsername(It.Is<string>(s => s.Contains("NotFound"))))
                .Returns((User?) null);
            userServiceMock
                .Setup(service => service
                    .CreateUser(It.IsAny<User>()))
                .Returns(new User(){Username = "Created"});
            return userServiceMock;
        }

    }
}