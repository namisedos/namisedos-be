using System;
using Moq;
using Namisedos_BE.Entities;
using Namisedos_BE.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory;
using Namisedos_BE.Helpers;
using Namisedos_BE.Models;
using Xunit;

namespace UnitTests
{
    public class UserRepositoryTests
    {
        [Fact]
        public void GetAll_UsersNotEmpty()
        {
            var dbContext = GetDatabaseContext();
            var userRepository = new UserRepository(dbContext);
            //Act
            var users = userRepository.GetAll();
            //Assert
            users.Count.Should().Be(10);
        }
        [Fact]
        public void GetAll_UsersEmpty()
        {
            var dbContext = GetDatabaseContext(new List<User>());
            var userRepository = new UserRepository(dbContext);
            //Act
            var users = userRepository.GetAll();
            //Assert
            users.Count.Should().Be(0);
        }
        [Fact]
        public void Add_UsersNotEmpty()
        {
            var dbContext = GetDatabaseContext();
            var userRepository = new UserRepository(dbContext);
            var user = new User {Username = "TestUsername", Password = "TestPassword", Email = "TestEmail"};
            var result = userRepository.Add(user);
            result.Should().NotBeNull();
            result.Username.Should().Be(user.Username);
            result.Id.Should().NotBe(Guid.Empty);
            result.HomeAddress.Should().BeNull();
            result.Trips.Should().BeNull();
            result.CreatedDate.Should().Be(user.CreatedDate);
            result.UpdatedDate.Should().Be(user.UpdatedDate);
        }
        [Fact]
        public void Add_Users_With_HomeAddress()
        {
            var dbContext = GetDatabaseContext();
            var userRepository = new UserRepository(dbContext);
            var user = new User { Username = "TestUsername", Password = "TestPassword", Email = "TestEmail" };
            user.HomeAddress = new Coordinates() {Latitude = 23, Longitude = 14};
            var result = userRepository.Add(user);
            result.Should().NotBeNull();
            result.HomeAddress.Should().NotBeNull();
            result.HomeAddress?.Latitude.Should().Be(23);
            result.HomeAddress?.Longitude.Should().Be(14);
        }
        [Fact]
        public void Add_Users_With_Trip()
        {
            var dbContext = GetDatabaseContext();
            var userRepository = new UserRepository(dbContext);
            var user = new User
            {
                Username = "TestUsername", Password = "TestPassword", Email = "TestEmail",
                Trips = new List<Trip>()
                {
                    new()
                    {
                        Stops = new List<Coordinates>()
                        {
                            new() {Latitude = 23, Longitude = 14}
                        },
                    }
                }
            };
            var result = userRepository.Add(user);
            result.Should().NotBeNull();
            result.Trips.Should().NotBeNull();
            result.Trips.Any(trip => trip.TripAuthor.Username == "TestUsername").Should().BeTrue();
            result.Trips.Any(trip => trip.TripAuthorId == result.Id).Should().BeTrue();

        }
        [Fact]
        public void Add_UsersEmpty()
        {
            var dbContext = GetDatabaseContext(new List<User>());
            var userRepository = new UserRepository(dbContext);
            var user = new User { Username = "TestUsername", Password = "TestPassword", Email = "TestEmail" };
            var result = userRepository.Add(user);
            result.Should().NotBeNull();
            result.Username.Should().Be(user.Username);
            result.Id.Should().NotBe(Guid.Empty);
            result.HomeAddress.Should().BeNull();
            result.Trips.Should().BeNull();
            result.CreatedDate.Should().Be(user.CreatedDate);
            result.UpdatedDate.Should().Be(user.UpdatedDate);


        }


        public static Mock<IUserRepository> GetRepositoryMock(Mock<IUserRepository>? userRepositoryMock = null)
        {
            userRepositoryMock ??= new Mock<IUserRepository>();
            userRepositoryMock.Setup(repo => repo.GetAll()).Returns(GetUsers());
            userRepositoryMock.Setup(repo => repo.Add(It.IsAny<User>())).Returns(new User());
            return userRepositoryMock;
        }
        private static DataContext GetDatabaseContext(List<User>? users = null)
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var databaseContext = new DataContext(options);
            databaseContext.Database.EnsureCreated();
            if (users != null)
            {
                foreach (var user in users)
                {
                    databaseContext.Users.Add(user);
                    databaseContext.SaveChanges();
                }
                return databaseContext;
            }

            for (var i = 1; i <= 10; i++)
            {
                databaseContext.Users.Add(new User()
                {
                    Id = Guid.NewGuid(),
                    Email = $"testuser{i}@example.com",
                    Username = $"testuser{i}",
                    Password = $"password{i}",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now
                });
                databaseContext.SaveChanges();
            }
            return databaseContext;
        }
        public static List<User> GetUsers(List<User>? users = null)
        {
            users ??= new List<User>();
            users.Add(
                new User()
                {
                    Username = "Found",
                    Email = "email@email.com"
                });
            users.Add(
                new User()
                {
                    Username = "Found2",
                    Email = "user1@user2.net"

                });
            return users;
        }
    }
}