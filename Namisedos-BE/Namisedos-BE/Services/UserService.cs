﻿using AutoMapper;
using Namisedos_BE.Entities;
using Namisedos_BE.Repositories;

namespace Namisedos_BE.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        public User? FindUserByUsername(string username)
        {
            var userEntity = _userRepository.GetAll().FirstOrDefault(user => user?.Username == username, null);
            return userEntity;
        }
        public User? GetById(Guid userId)
        {
            var userEntity = _userRepository.GetById(userId);
            return userEntity;
        }
        public List<User> GetAll()
        {
            var userEntity = _userRepository.GetAll();
            return userEntity;
        }
        public User CreateUser(User user)
        {
            var createdEntity = _userRepository.Add(user);
            return createdEntity;
        }
        public User? UpdateUser(User user)
        {
            var userToUpdate = _userRepository.GetById(user.Id);
            if (userToUpdate == null)
            {
                return null;
            }

            userToUpdate.Password = user.Password;
            userToUpdate.Email = user.Email;
            userToUpdate.HomeAddress = user.HomeAddress;
            userToUpdate.UpdatedDate = DateTime.Now;
            var updatedEntity = _userRepository.Update(userToUpdate);
            return updatedEntity;
        }
    }
}
