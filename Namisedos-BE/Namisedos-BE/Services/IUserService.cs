﻿using Namisedos_BE.Entities;

namespace Namisedos_BE.Services;

public interface IUserService
{
    User? FindUserByUsername(string username);
    User CreateUser(User user);
    User UpdateUser(User user);
    List<User> GetAll();
    User? GetById(Guid userId);
}