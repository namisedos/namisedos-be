using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Namisedos_BE.Attributes;
using Namisedos_BE.Entities;
using Namisedos_BE.Models;
using Namisedos_BE.Services;

namespace Namisedos_BE.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("[action]")]
        public string Test()
        {
            return "Veikia";
        }

        [HttpGet]
        [Authorize]
        [Route("[action]")]
        public string TestSecret()
        {
            return "Veikia Secret";
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Register(RegisterRequestModel registerRequestModel)
        {
            // validate
            if (_userService.FindUserByUsername(registerRequestModel.Username) != null)
                return BadRequest(new
                    {field = "username", status = "400", message = "User with this username already exists"}
                );

            var user = _mapper.Map<User>(registerRequestModel);
            user = _userService.CreateUser(user);
            return Ok(user);
        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Login(LoginRequestModel loginRequestModel)
        {
            var user = _userService.FindUserByUsername(loginRequestModel.Username);
            // validate
            if (user == null)
                return BadRequest(new
                    {status = "400", message = "Wrong username"}
                );
            if (user.Password != loginRequestModel.Password)
                return BadRequest(new
                    {status = "400", message = "Wrong password"}
                );
            return Ok(user);

        }

        [HttpPost]
        [Route("[action]")]
        public IActionResult Update(UpdateRequestModel updateRequestModel)
        {
            // validate
            var userInDb = _userService.GetById(updateRequestModel.Id);
            if (userInDb == null)
                return BadRequest(new
                    {field = "Id", status = "400", message = "User with this id does not exist"}
                );
            var user = _mapper.Map<User>(updateRequestModel);
            user = _userService.UpdateUser(user);
            return Ok(user);
        }

        [HttpGet]
        [Route("[action]")]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }
    }
}