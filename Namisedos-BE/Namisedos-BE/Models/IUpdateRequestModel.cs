﻿namespace Namisedos_BE.Models;

public interface IUpdateRequestModel
{
    Guid Id { get; set; }
    string Email { get; set; }
    string Username { get; set; }
    string Password { get; set; }
    string HomeAddressString { get; set; }
    string CarModelString { get; set; }
}