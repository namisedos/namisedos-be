﻿namespace Namisedos_BE.Models;

public interface ILoginRequestModel
{
    string Username { get; }
    string Password { get; }
}