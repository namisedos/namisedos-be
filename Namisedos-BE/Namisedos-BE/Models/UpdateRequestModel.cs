﻿using System.ComponentModel.DataAnnotations;
namespace Namisedos_BE.Models
{
    public class UpdateRequestModel : IUpdateRequestModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
        public string? HomeAddressString { get; set; }

        public string? CarModelString { get; set; }
    }
}
