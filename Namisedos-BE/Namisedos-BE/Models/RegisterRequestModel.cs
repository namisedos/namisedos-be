﻿using System.ComponentModel.DataAnnotations;
namespace Namisedos_BE.Models
{
    public class RegisterRequestModel : IRegisterRequestModel
    {
        public RegisterRequestModel(string email, string username, string password)
        {
            Email = email;
            Username = username;
            Password = password;
        }
        public RegisterRequestModel()
        {
           
        }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
