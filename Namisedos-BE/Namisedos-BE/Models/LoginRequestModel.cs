﻿using System.ComponentModel.DataAnnotations;
namespace Namisedos_BE.Models
{
    public class LoginRequestModel : ILoginRequestModel
    {
        public LoginRequestModel(string username, string password)
        {
            Username = username;
            Password = password;
        }

        [Required]
        public string Username { get; }

        [Required]
        public string Password { get; }
    }
}
