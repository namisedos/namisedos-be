﻿namespace Namisedos_BE.Models;

public interface IRegisterRequestModel
{
    string Email { get; set; }
    string Username { get; set; }
    string Password { get; set; }
}