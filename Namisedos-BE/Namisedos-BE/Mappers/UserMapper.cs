﻿using AutoMapper;
using Namisedos_BE.Entities;
using Namisedos_BE.Models;

namespace Namisedos_BE.Mappers
{
    public class UserMapper : Profile
    {
        public UserMapper()
        {
            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<RegisterRequestModel, UserModel>().ReverseMap();
            CreateMap<RegisterRequestModel, User>().ReverseMap();
            CreateMap<UpdateRequestModel, User>().ReverseMap();

        }

    }
}
