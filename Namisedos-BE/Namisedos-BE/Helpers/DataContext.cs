﻿using Microsoft.EntityFrameworkCore;
using Namisedos_BE.Entities;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace Namisedos_BE.Helpers

{
    public class DataContext : DbContext
    {
        private readonly string _connectionString;
        public DataContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        public DataContext()
        {
        }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            UseSqlServerConnectionString(optionsBuilder);
        }

        public void UseSqlServerConnectionString(DbContextOptionsBuilder optionsBuilder)
        {
            if (_connectionString == null) return;
                optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Trip> Trips { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Coordinates> Coordinates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().Property(b => b.CreatedDate).ValueGeneratedOnAdd();
            modelBuilder.Entity<User>().Property(b => b.UpdatedDate).ValueGeneratedOnUpdate();
            modelBuilder.Entity<User>().Property(b => b.Username).IsRequired().HasMaxLength(50);
            modelBuilder.Entity<User>().Property(b => b.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Trip>()
                .HasOne(p => p.TripAuthor)
                .WithMany(c => c.Trips)
                .HasForeignKey(p => p.TripAuthorId);
            modelBuilder.Entity<Coordinates>()
                .HasOne(p => p.Trip)
                .WithMany(c => c.Stops)
                .HasForeignKey(p => p.TripId);
            modelBuilder.Entity<User>()
                .HasOne(p => p.HomeAddress)
                .WithOne(c => c.User)
                .HasForeignKey<Coordinates>(c => c.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Trip>()
                .HasOne(p => p.Car)
                .WithMany(c => c.Trip);
        }
    }
}
