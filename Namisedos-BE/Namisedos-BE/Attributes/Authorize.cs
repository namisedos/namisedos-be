﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using Microsoft.Extensions.Primitives;
using Namisedos_BE.Models;

namespace Namisedos_BE.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class Authorize : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext authorizationFilterContext)
        {
            authorizationFilterContext.HttpContext.Request.Headers.TryGetValue("x-api-key", out var authTokens);
            var token = authTokens.FirstOrDefault();

            if (token != null)
            {
                if (IsValidToken(token))
                {
                    authorizationFilterContext.HttpContext.Response.Headers.Add("authToken", token);
                    authorizationFilterContext.HttpContext.Response.Headers.Add("AuthStatus", "Authorized");

                    authorizationFilterContext.HttpContext.Response.Headers.Add("storeAccessiblity", "Authorized");

                    return;
                }

                authorizationFilterContext.HttpContext.Response.Headers.Add("authToken", token);
                authorizationFilterContext.HttpContext.Response.Headers.Add("AuthStatus", "NotAuthorized");

                authorizationFilterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                authorizationFilterContext.HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>()!.ReasonPhrase = "Not Authorized";
                authorizationFilterContext.Result = new JsonResult("NotAuthorized")
                {
                    Value = new
                    {
                        Status = "Error",
                        Message = "Invalid Token"
                    },
                };

            }
            else
            {
                authorizationFilterContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.ExpectationFailed;
                authorizationFilterContext.HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>()!.ReasonPhrase = "Please Provide authToken";
                authorizationFilterContext.Result = new JsonResult("Please Provide authToken")
                {
                    Value = new
                    {
                        Status = "Error",
                        Message = "Please Provide authToken"
                    },
                };
            }
        }

        public bool IsValidToken(string authToken)
        {
            return authToken == ApiKey.Key; ;
        }
    }
}
