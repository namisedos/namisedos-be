﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Namisedos_BE.Attributes;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Namisedos_BE.Swagger
{
    public class AuthorizationOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var actionMetadata = context.ApiDescription.ActionDescriptor.EndpointMetadata;
            var isAuthorized = actionMetadata.Any(metadataItem => metadataItem is Authorize);
            var allowAnonymous = actionMetadata.Any(metadataItem => metadataItem is AllowAnonymousAttribute);

            if (!isAuthorized || allowAnonymous) return;
            operation.Parameters ??= new List<OpenApiParameter>();
            operation.Parameters.Add(new OpenApiParameter()
            {
                Name = "x-api-key",
                In = ParameterLocation.Header,
                Description = "Authorization by x-api-key inside request's header",
            });
            operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
            operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden" });

            operation.Security = new List<OpenApiSecurityRequirement>();

            var key = new OpenApiSecurityScheme()
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "ApiKey"
                },
                In = ParameterLocation.Header
            };
            operation.Security.Add(new OpenApiSecurityRequirement
            {
                { key, new List<string>() }
            });
        }
    }
}