﻿using Namisedos_BE.Models;

namespace Namisedos_BE.Entities
{
    public class Trip : BaseEntity
    {

        public Guid Id { get; set; }
        public IList<Coordinates> Stops { get; set; }
        public User TripAuthor { get; set; }
        public Guid TripAuthorId { get; set; }
        public Car? Car { get; set; }
        public Guid? CarId { get; set; }

    }
}
