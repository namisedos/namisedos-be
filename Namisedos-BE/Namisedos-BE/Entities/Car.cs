﻿namespace Namisedos_BE.Entities
{
    public class Car
    {
        public Guid Id { get; set; }
        public string? CarMake { get; set; }
        public string? CarModel { get; set; }
        public int? CarYear { get; set; }
        public double? CarRange { get; set; }
        public User? User { get; set; }
        public List<Trip> Trip { get; set; }
        public Guid? UserId { get; set; }
        public Guid? TripId { get; set; }


    }
}
