﻿namespace Namisedos_BE.Entities
{
    public class Coordinates : BaseEntity
    {
        public Guid Id { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public User? User { get; set; }
        public Guid? UserId { get; set; }
        public Trip? Trip { get; set; }
        public Guid? TripId { get; set; }


    }
}
