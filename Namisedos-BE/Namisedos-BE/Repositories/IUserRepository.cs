﻿using Namisedos_BE.Entities;

namespace Namisedos_BE.Repositories;

public interface IUserRepository
{
    User Add(User user);
    List<User> GetAll();
    User? GetById(Guid id);
    User Update(User user);

}