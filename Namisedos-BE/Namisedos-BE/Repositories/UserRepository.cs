﻿using Namisedos_BE.Entities;
using Namisedos_BE.Helpers;
using Namisedos_BE.Models;

namespace Namisedos_BE.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;

        public UserRepository(DataContext dataContext)
        {
            _context = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
        }

        public User Add(User user)
        {
            var createdEntity = _context.Users.Add(user).Entity;
            _context.SaveChanges();
            return createdEntity;
        }
        public List<User> GetAll()
        {
            return _context.Users.ToList();
        }

        public User? GetById(Guid id)
        {
            return _context.Users.Find(id);
        }

        public User Update(User user)
        {
            var updatedEntity = _context.Users.Update(user).Entity;
            _context.SaveChanges();
            return updatedEntity;
        }

    }
}
