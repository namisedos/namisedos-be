using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Namisedos_BE.Helpers;
using Namisedos_BE.Models;
using Namisedos_BE.Repositories;
using Namisedos_BE.Services;
using Namisedos_BE.Swagger;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var env = builder.Environment;
services.AddCors();
services.AddControllers();
services.AddEndpointsApiExplorer();
services.AddOptions();
services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Api Key Auth", Version = "v1" });
    c.OperationFilter<AuthorizationOperationFilter>();
    c.AddSecurityDefinition("ApiKey", new OpenApiSecurityScheme()
    {
        Name = "x-api-key",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Description = "Authorization by x-api-key inside request's header",
    });
});
services.AddDbContext<DataContext>();
services.AddAutoMapper(typeof(Program));
services.AddScoped<IUserService, UserService>();
services.AddScoped<IUserRepository, UserRepository>();
services.AddSingleton(_ => new DataContext(builder.Configuration.GetConnectionString("DefaultConnection")));




var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "NamisedosAPI");
        c.DocumentTitle = "NamisedosAPI";
        //c.SupportedSubmitMethods();
    });
}

app.UseHttpsRedirection();
app.UseCors(c => c.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
app.UseAuthorization();
//app.UseMiddleware<ApiKeyMiddleware>();
app.MapControllers();

app.Run();
